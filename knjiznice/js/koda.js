
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */

function pokliciGeneriranjePodatkov(){

	$('#generirajNakljucno').text("");
	generirajPodatke(1);
	generirajPodatke(2);
	generirajPodatke(3);
}
 
function generirajPodatke(stPacienta) {
  ehrId = "";
	
  	if(stPacienta == 1){
		ehrId = "7dad6fbf-8ae6-4414-aca8-0f81143e44be";
		var tezz = 105;
		var viss = 180;
		var siss = Math.round((Math.random() * 100) /2+60);
		var diss = Math.round((Math.random() * 100) /3+40);
		dodajPodatkeEHR(ehrId,viss,tezz,siss,diss);
		$('#generirajNakljucno').append("<hr><p class=\"lead\">Generirani podatki:</p>");
		$('#generirajNakljucno').append("Miha Novak: Višina: "+viss+"cm, Teža: "+tezz+"kg, Tlak: "+siss+"/"+diss+"mmHg");
	}
	else if(stPacienta == 2){
		ehrId = "ef8e6d49-af85-49c6-ad49-2dd9a27161ba";
		var tezz = 163;
		var viss = 193;
		var siss = Math.round((Math.random() * 100) /2+110);
		var diss = Math.round((Math.random() * 100) /3+70);
		dodajPodatkeEHR(ehrId,viss,tezz,siss,diss);
		$('#generirajNakljucno').append("</br>Michael Phelps: Višina: "+viss+"cm, Teža: "+tezz+"kg, Tlak: "+siss+"/"+diss+"mmHg");
	}
	else if(stPacienta == 3){
		ehrId = "db3d3030-0a7f-4ad0-be50-71b9fcb19212";
		var tezz = 49;
		var viss = 172;
		var siss = Math.round((Math.random() * 100) /2+80);
		var diss = Math.round((Math.random() * 100) /3+65);
		dodajPodatkeEHR(ehrId,viss,tezz,siss,diss);
		$('#generirajNakljucno').append("</br>Cara Delevingne: Višina: "+viss+"cm, Teža: "+tezz+"kg, Tlak: "+siss+"/"+diss+"mmHg");
	}
	
	return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function dodajPodatke(){
	var vis = $('#visina').val();
	var tez = $('#teza').val();
	var sis = $('#sistol').val();
	var dis = $('#diastol').val();
	var idd = $('#EHRID').val();
	dodajPodatkeEHR(idd,vis,tez,sis,dis);
}

function dodajPodatkeEHR(ehrId, visina, teza, sistol, distol){
	var sessid = getSessionId();
	$.ajaxSetup({
	headers: {
		"Ehr-Session":sessid
	}});
	var data = {
		"ctx/language": "en",
		"ctx/territory": "SI",
		"ctx/time": "2016-01-01",
		"vital_signs/height_length/any_event/body_height_length": visina,
        	"vital_signs/body_weight/any_event/body_weight": teza,
        	"vital_signs/blood_pressure/any_event/systolic": sistol,
        	"vital_signs/blood_pressure/any_event/diastolic": distol
	};
	var parampampam = {
		ehrId: ehrId,
		templateId: 'Vital Signs',
		format: 'FLAT',
		committer: 'jst'
	};
	$.ajax({
		url: baseUrl + "/composition?" + $.param(parampampam),
		type: 'POST',
		contentType: 'application/json',
		data: JSON.stringify(data),
		success: function(odgovor){
			
	$('#generirajObvestilo').text("Podatki dodani");
		},
		error: function(error){
	$('#generirajNakljucno').text("napaka");
			console.log("napaka"+error);
		}
	});
}

function dodajEHR(){
	dodajVBazo($('#dodajIme').val(),$('#dodajPriimek').val(),$('#dodajDatum').val());
}

function dodajVBazo(ime2, priimek2, datum2){
	sessId = getSessionId();
	$.ajaxSetup({
	headers: {
		"Ehr-Session": sessId
	}});
	var odg = $.ajax({
		url: baseUrl + '/ehr',
		async: false,
		type: 'POST',
		success: function(data) {
			var ehrId = data.ehrId;
			var partyData = {
				firstNames: ime2,
				lastNames: priimek2,
				dateOfBirth: datum2,
				partyAdditionalInfo: [{
					key: "ehrId", value: ehrId
				}]
		};
		$.ajax({
			url: baseUrl + "/demographics/party",
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(partyData),
			success: function(party){
				$('#napisiObvestilo').text('Vaš EHR ID: ' + ehrId);
			},
			error: function(error){
				$('#napisiObvestilo').text('Napaka pri vnosu.');
			}
		});
	}
	});
}

function obdelajPodatke(){
sessionId = getSessionId();
	var	iddd = $('#ERHID2').val();
	console.log(iddd);
	$('#kasnejeIzbisi').text("");
	if (!iddd || iddd.trim().length == 0) {
		$('#kasnejeIzbisi').text("manjka ER-HR ID");
		return;
	}
	$.ajax({
            url: baseUrl + "/view/" + iddd + "/" + "blood_pressure?" + $.param({
                limit: 25
            }),
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },
		success: function(tlk){
	$.ajax({
                    url: baseUrl + "/view/" + iddd + "/" + "weight",
                    type: 'GET',
                    headers: {
                        "Ehr-Session": sessionId
                    },
		success: function(tez){
	$.ajax({
                    url: baseUrl + "/view/" + iddd + "/" + "height",
                    type: 'GET',
                    headers: {
                        "Ehr-Session": sessionId
                    },	
		success: function(vis){
			prikaziPodatke(tlk,tez,vis);
		},
	error: function(){
		console.log("napaka");
	}
});
	},
	error: function(){
		console.log("napaka");
	}
});
	},
	error: function(){
		console.log("napaka");
	}
});
	
}

function prikaziPodatke(tlk,tez,vis) {
	var i = vis.length-1;
	var teza = tez[i].weight;
	var visina = vis[i].height;
	
	var izracunanBMI;
	izracunanBMI = teza/((visina/100)*(visina/100));
	
	izracunanBMI = izracunanBMI.toFixed(2);
	
	var izpisBMI = "<p class=\"lead\"><strong>Vaš BMI je <font size=\"7\">" + izracunanBMI + "</font></strong></p>";
	
	$('#kasnejeIzbisi').append(izpisBMI);
	$('#kasnejeIzbisi').append('<p><font size="7"><strong> &#8595;</strong></font></p>');
	
	var mojBMI;
	
	if(izracunanBMI <= 18.5) {
		mojBMI = "prenizek";
	    $('#kasnejeIzbisi').append("<p><i><font size=\"7\">Vaš BMI je PRENIZEK</font></i></p><hr>");
	} else if(izracunanBMI > 18.5 && izracunanBMI <= 24.9) {
		mojBMI = "normalen";
	    $('#kasnejeIzbisi').append("<p><i><font size=\"7\">Vaš BMI je NORMALEN</font></i></p><hr>");
	} else if(izracunanBMI > 24.9 && izracunanBMI <= 29.9) {
		mojBMI = "previsok"
	    $('#kasnejeIzbisi').append("<p><i><font size=\"7\">Vaš BMI je PREVISOK</font></i></p><hr>");
	} else {
		mojBMI = "zelo previsok";
	    $('#kasnejeIzbisi').append("<p><i><font size=\"7\">DEBELOST</font></i></p><hr>");
	}
	
	var slika1 = '<img src="./knjiznice/bmi.png" height="42" class="center">';
	var slika2in3 = '<p></p><div class="row"><div class="col-xs-6"><img src="./knjiznice/tabela.png" height="200" class="center"></div><div class="col-xs-6"><img src="./knjiznice/obesity.png" height="350" class="center"></div></div>';
	
	var slika2 = '<p class="lead">Tabela BMI</p><img src="./knjiznice/tabela.png" height="250" class="center"><hr>';
	
	$('#kasnejeIzbisi').append(slika2);
	
	
	

    
}


$(window).load(function() {
    $("#vzorcnipacienti").change(function() {
        $('#ERHID2').val($('#vzorcnipacienti').val());
    });

});

